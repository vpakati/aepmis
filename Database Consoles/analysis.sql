USE aepmis;
# 查询用户总数
DROP PROCEDURE IF EXISTS query_num_of_system_users;
CREATE PROCEDURE query_num_of_system_users(
    OUT num_of_system_users INT
)
BEGIN
    DECLARE num_of_staff INT;
    DECLARE num_of_student INT;

    SELECT COUNT(staff_id)
    INTO num_of_staff
    FROM staff;

    SELECT COUNT(student_id)
    INTO num_of_student
    FROM student;

    SELECT num_of_staff + num_of_student
    INTO num_of_system_users;
END;

# SELECT daily_health_report # 今日已填写健康日报的用户占总用户的百分比，即目前全校健康日报的填写进度
DROP PROCEDURE IF EXISTS query_percentage_of_reported_users_today;
CREATE PROCEDURE query_percentage_of_reported_users_today(
    OUT percentage_of_reported_users_today DECIMAL(5, 2)
)
BEGIN
    DECLARE num_of_system_users INT;
    DECLARE num_of_reported_staffs_today INT;
    DECLARE num_of_reported_students_today INT;

    CALL query_num_of_system_users(num_of_system_users);

    SELECT COUNT(staff.staff_id)
    INTO num_of_reported_staffs_today
    FROM staff
             LEFT OUTER JOIN daily_health_report
                             ON staff.staff_id = daily_health_report.signer_id
    WHERE DATE(daily_health_report.time_of_sign) = CURDATE();

    SELECT COUNT(student.student_id)
    INTO num_of_reported_students_today
    FROM student
             LEFT OUTER JOIN daily_health_report
                             ON student.student_id = daily_health_report.signer_id
    WHERE DATE(daily_health_report.time_of_sign) = CURDATE();

    SELECT (num_of_reported_staffs_today + num_of_reported_students_today) / num_of_system_users
    INTO percentage_of_reported_users_today;
END;

# ---------------------------
# SELECT student staff # 已确诊的用户
DROP PROCEDURE IF EXISTS query_those_who_have_been_diagnosed;
CREATE PROCEDURE query_those_who_have_been_diagnosed()
BEGIN
    SELECT '学生'                                                 AS '身份',
           IFNULL(student.student_id, '')                       AS '学号/教职工号',
           IFNULL(student.name, '')                             AS '姓名',
           IFNULL(student.phone_number, '')                     AS '手机号',
           IFNULL(student.mailbox, '')                          AS '邮箱',
           IFNULL(IF(student.is_diagnosed = 'Y', '是', '否'), '') AS '是否已确诊'
    FROM student
    WHERE student.is_diagnosed = 'Y'
    UNION
    SELECT '教职工',
           IFNULL(staff.staff_id, ''),
           IFNULL(staff.name, ''),
           IFNULL(staff.phone_number, ''),
           IFNULL(staff.mailbox, ''),
           IFNULL(IF(staff.is_diagnosed = 'Y', '是', '否'), '')
    FROM staff
    WHERE staff.is_diagnosed = 'Y';
END;

# SELECT student staff # 已确诊的用户占总用户的百分比
DROP PROCEDURE IF EXISTS query_percentage_of_users_who_have_been_diagnosed;
CREATE PROCEDURE query_percentage_of_users_who_have_been_diagnosed(
    OUT percentage_of_users_who_have_been_diagnosed DECIMAL(5, 2)
)
BEGIN
    DECLARE num_of_system_users INT;
    DECLARE num_of_students_who_have_been_diagnosed INT;
    DECLARE num_of_staffs_who_have_been_diagnosed INT;

    CALL query_num_of_system_users(num_of_system_users);

    SELECT COUNT(student.student_id)
    INTO num_of_students_who_have_been_diagnosed
    FROM student
    WHERE student.is_diagnosed = 'Y';

    SELECT COUNT(staff.staff_id)
    INTO num_of_staffs_who_have_been_diagnosed
    FROM staff
    WHERE staff.is_diagnosed = 'Y';

    SELECT (num_of_students_who_have_been_diagnosed + num_of_staffs_who_have_been_diagnosed) / num_of_system_users
    INTO percentage_of_users_who_have_been_diagnosed;
END;
# ---------------------------

# DROP PROCEDURE IF EXISTS test;
# CREATE PROCEDURE test()
# BEGIN
#     DECLARE percentage_of_users_who_have_been_diagnosed DECIMAL(5, 2);
#
#     CALL query_percentage_of_users_who_have_been_diagnosed(percentage_of_users_who_have_been_diagnosed);
#
#     SELECT percentage_of_users_who_have_been_diagnosed;
# END;
# CALL test();