# # skip
# USE aepmis;
#
# # SELECT permission
# SELECT *
# FROM permission;
#
# # INSERT permission
# INSERT INTO permission(permission_id, permission_name, comment)
# VALUES ('03', '游客', '这种权限是仅用于系统演示');
#
# # UPDATE permission
# UPDATE permission
# SET permission_id = '04', permission_name = '访客', comment = '这种权限是仅用于系统演示！'
# WHERE permission_id = '03';
#
# # DELETE permission
# DELETE FROM permission
# where permission_id = '04';