# d-列名
# d-IFNULL
USE aepmis;

# SELECT announcement # for student
DROP PROCEDURE IF EXISTS query_announcement_for_student;
CREATE PROCEDURE query_announcement_for_student()
BEGIN
    SELECT IFNULL(announcement.post_time, '')   AS '发布时间',
           IFNULL(announcement.update_time, '') AS '更新时间',
           IFNULL(staff.name, '')               AS '发布人',
           IFNULL(announcement.content, '')     AS '内容'
    FROM announcement
             INNER JOIN staff
                        ON announcement.publisher_id = staff.staff_id
             INNER JOIN recipient_group
                        ON announcement.recipient_group_id = recipient_group.recipient_group_id
    WHERE recipient_group.recipient_group_name = '全体'
       OR recipient_group.recipient_group_name = '全体学生'
    ORDER BY announcement.post_time DESC;
END;
# CALL query_announcement_for_student();

# SELECT announcement # for student # latest
DROP PROCEDURE IF EXISTS query_latest_announcement_for_student;
CREATE PROCEDURE query_latest_announcement_for_student()
BEGIN
    SELECT IFNULL(announcement.post_time, '')   AS '发布时间',
           IFNULL(announcement.update_time, '') AS '更新时间',
           IFNULL(staff.name, '')               AS '发布人',
           IFNULL(announcement.content, '')     AS '内容'
    FROM announcement
             INNER JOIN staff
                        ON announcement.publisher_id = staff.staff_id
             INNER JOIN recipient_group
                        ON announcement.recipient_group_id = recipient_group.recipient_group_id
    WHERE recipient_group.recipient_group_name = '全体'
       OR recipient_group.recipient_group_name = '全体学生'
    ORDER BY announcement.post_time DESC
    LIMIT 1;
END;
# CALL query_latest_announcement_for_student();

# SELECT announcement # for staff
DROP PROCEDURE IF EXISTS query_announcement_for_staff;
CREATE PROCEDURE query_announcement_for_staff()
BEGIN
    SELECT IFNULL(announcement.post_time, '')   AS '发布时间',
           IFNULL(announcement.update_time, '') AS '更新时间',
           IFNULL(staff.name, '')               AS '发布人',
           IFNULL(announcement.content, '')     AS '内容'
    FROM announcement
             INNER JOIN staff
                        ON announcement.publisher_id = staff.staff_id
             INNER JOIN recipient_group
                        ON announcement.recipient_group_id = recipient_group.recipient_group_id
    WHERE recipient_group.recipient_group_name = '全体'
       OR recipient_group.recipient_group_name = '全体教职工'
    ORDER BY announcement.post_time DESC;
END;
# CALL query_announcement_for_staff();

# SELECT announcement # for staff # latest
DROP PROCEDURE IF EXISTS query_latest_announcement_for_staff;
CREATE PROCEDURE query_latest_announcement_for_staff()
BEGIN
    SELECT IFNULL(announcement.post_time, '')   AS '发布时间',
           IFNULL(announcement.update_time, '') AS '更新时间',
           IFNULL(staff.name, '')               AS '发布人',
           IFNULL(announcement.content, '')     AS '内容'
    FROM announcement
             INNER JOIN staff
                        ON announcement.publisher_id = staff.staff_id
             INNER JOIN recipient_group
                        ON announcement.recipient_group_id = recipient_group.recipient_group_id
    WHERE recipient_group.recipient_group_name = '全体'
       OR recipient_group.recipient_group_name = '全体教职工'
    ORDER BY announcement.post_time DESC
    LIMIT 1;
END;
# CALL query_latest_announcement_for_staff();

# SELECT announcement # for_admin_or_above
DROP PROCEDURE IF EXISTS query_announcement_for_admin_or_above;
CREATE PROCEDURE query_announcement_for_admin_or_above()
BEGIN
    SELECT IFNULL(announcement.post_time, '')               AS '发布时间',
           IFNULL(announcement.update_time, '')             AS '更新时间',
           IFNULL(staff.name, '')                           AS '发布人',
           IFNULL(recipient_group.recipient_group_name, '') AS '接收群体名',
           IFNULL(announcement.content, '')                 AS '内容'
    FROM announcement
             INNER JOIN staff
                        ON announcement.publisher_id = staff.staff_id
             INNER JOIN recipient_group
                        ON announcement.recipient_group_id = recipient_group.recipient_group_id
    ORDER BY announcement.post_time DESC;
END;
# CALL query_announcement_for_admin_or_above();

# SELECT announcement # for_admin_or_above # latest
DROP PROCEDURE IF EXISTS query_latest_announcement_for_admin_or_above;
CREATE PROCEDURE query_latest_announcement_for_admin_or_above()
BEGIN
    SELECT IFNULL(announcement.post_time, '')               AS '发布时间',
           IFNULL(announcement.update_time, '')             AS '更新时间',
           IFNULL(staff.name, '')                           AS '发布人',
           IFNULL(recipient_group.recipient_group_name, '') AS '接收群体名',
           IFNULL(announcement.content, '')                 AS '内容'
    FROM announcement
             INNER JOIN staff
                        ON announcement.publisher_id = staff.staff_id
             INNER JOIN recipient_group
                        ON announcement.recipient_group_id = recipient_group.recipient_group_id
    ORDER BY announcement.post_time DESC
    LIMIT 1;
END;
# CALL query_latest_announcement_for_admin_or_above();

# INSERT announcement //d
DROP PROCEDURE IF EXISTS insert_into_announcement;
CREATE PROCEDURE insert_into_announcement(
    IN publisher_id CHAR(5),
    IN recipient_group_name VARCHAR(10),
    IN content LONGTEXT
)
BEGIN
    DECLARE recipient_group_id CHAR(2);

    SELECT recipient_group.recipient_group_id
    INTO recipient_group_id
    FROM recipient_group
    WHERE recipient_group.recipient_group_name = recipient_group_name;

    INSERT INTO announcement (publisher_id, post_time, recipient_group_id, content)
    VALUES (publisher_id, NOW(), recipient_group_id, content);
END;
# CALL insert_into_announcement('00100', '全体', '疫情尚未完全消退，请各位师生减少不必要的外出。');

# UPDATE announcement //d
DROP PROCEDURE IF EXISTS update_announcement;
CREATE PROCEDURE update_announcement(
    IN recipient_group_name VARCHAR(10),
    IN content LONGTEXT,
    IN publisher_id CHAR(5),
    IN post_time DATETIME
)
BEGIN
    DECLARE recipient_group_id CHAR(2);

    SELECT recipient_group.recipient_group_id
    INTO recipient_group_id
    FROM recipient_group
    WHERE recipient_group.recipient_group_name = recipient_group_name;

    UPDATE announcement
    SET announcement.recipient_group_id = recipient_group_id,
        announcement.content            = content,
        announcement.update_time        = NOW()
    WHERE announcement.publisher_id = publisher_id
      AND announcement.post_time = post_time;
END;
# CALL update_announcement('全体', '精准防疫，我们必将胜利!', '00100', '2021-03-26 16:52:58');

# DELETE announcement //d
DROP PROCEDURE IF EXISTS delete_announcement;
CREATE PROCEDURE delete_announcement(
    IN publisher_id CHAR(5),
    IN post_time DATETIME
)
BEGIN
    DELETE
    FROM announcement
    WHERE announcement.publisher_id = publisher_id
      AND announcement.post_time = post_time;
END;
# CALL delete_announcement('00102', '2021-05-01 21:50:21');
