# d-列名
# d-IFNULL
USE aepmis;

# SELECT permission_assignment # by_staff_id
DROP PROCEDURE IF EXISTS query_permission_assignment_by_staff_id;
CREATE PROCEDURE query_permission_assignment_by_staff_id(IN staff_id CHAR(5))
BEGIN
    SELECT IFNULL(staff.name, '')                    AS '教职工名',
           IFNULL(permission.permission_name, '无')   AS '权限名',
           IFNULL(permission_assignment.comment, '') AS '备注'
    FROM permission_assignment
             INNER JOIN permission
                        ON permission_assignment.permission_id = permission.permission_id
             RIGHT OUTER JOIN staff
                              ON permission_assignment.staff_id = staff.staff_id
    WHERE permission_assignment.staff_id = staff_id;
END;
# CALL query_permission_assignment_by_staff_id('00102');

# SELECT permission_assignment
DROP PROCEDURE IF EXISTS query_permission_assignment;
CREATE PROCEDURE query_permission_assignment()
BEGIN
    SELECT IFNULL(staff.name, '')                    AS '教职工名',
           IFNULL(permission.permission_name, '无')   AS '权限名',
           IFNULL(permission_assignment.comment, '') AS '备注'
    FROM permission_assignment
             INNER JOIN permission
                        ON permission_assignment.permission_id = permission.permission_id
             RIGHT OUTER JOIN staff
                              ON permission_assignment.staff_id = staff.staff_id;
END;
# CALL query_permission_assignment();

# # INSERT permission_assignment
# DROP PROCEDURE IF EXISTS insert_into_permission_assignment;
# CREATE PROCEDURE insert_into_permission_assignment(
#     IN staff_id CHAR(5),
#     IN permission_name VARCHAR(10),
#     IN comment LONGTEXT
# )
# BEGIN
#     DECLARE permission_id CHAR(2);
#
#     SELECT permission.permission_id
#     INTO permission_id
#     FROM permission
#     WHERE permission.permission_name = permission_name;
#
#     INSERT INTO permission_assignment(staff_id, permission_id, comment)
#     VALUES (staff_id, permission_id, comment);
# END;
# CALL insert_into_permission_assignment('00100', '超级管理员', '超级管理员');
# CALL insert_into_permission_assignment('00103', '管理员', '111');

# INSERT permission_assignment # as_admin
DROP PROCEDURE IF EXISTS insert_into_permission_assignment_as_admin;
CREATE PROCEDURE insert_into_permission_assignment_as_admin(
    IN staff_id CHAR(5),
#     IN permission_name VARCHAR(10),
    IN comment LONGTEXT
)
BEGIN
    DECLARE permission_id CHAR(2);

    SELECT permission.permission_id
    INTO permission_id
    FROM permission
    WHERE permission.permission_name = '管理员';

    INSERT INTO permission_assignment(staff_id, permission_id, comment)
    VALUES (staff_id, permission_id, comment);
END;
# CALL insert_into_permission_assignment_as_admin('00103', '111');

# # UPDATE permission_assignment
# DROP PROCEDURE IF EXISTS update_permission_assignment;
# CREATE PROCEDURE update_permission_assignment(
#     IN staff_id CHAR(5),
#     IN name_of_new_permission VARCHAR(10),
#     IN comment LONGTEXT
# )
# BEGIN
#     DECLARE permission_id CHAR(2);
#
#     SELECT permission.permission_id
#     INTO permission_id
#     FROM permission
#     WHERE permission.permission_name = name_of_new_permission;
#
#     UPDATE permission_assignment
#     SET permission_assignment.staff_id      = staff_id,
#         permission_assignment.permission_id = permission_id,
#         permission_assignment.comment       = comment
#     WHERE permission_assignment.staff_id = staff_id;
# END;
# CALL update_permission_assignment('00103', '超级管理员', 'comment');

# # DELETE permission_assignment # 这个存储过程将删除指定教职工的所有权限
# DROP PROCEDURE IF EXISTS delete_permission_assignment;
# CREATE PROCEDURE delete_permission_assignment(
#     IN staff_id CHAR(5)
# )
# BEGIN
#     DELETE
#     FROM permission_assignment
#     WHERE permission_assignment.staff_id = staff_id;
# END;
# # CALL delete_permission_assignment('00103');

# DELETE permission_assignment # as_admin
DROP PROCEDURE IF EXISTS delete_permission_assignment_as_admin;
CREATE PROCEDURE delete_permission_assignment_as_admin(
    IN staff_id CHAR(5)
)
BEGIN
    DECLARE permission_id CHAR(2);

    SELECT permission.permission_id
    INTO permission_id
    FROM permission
    WHERE permission.permission_name = '管理员';

    DELETE
    FROM permission_assignment
    WHERE permission_assignment.staff_id = staff_id
      AND permission_assignment.permission_id = permission_id;
END;
# CALL delete_permission_assignment_as_admin('00103');