# d-列名
# d-IFNULL
USE aepmis;

# SELECT student
DROP PROCEDURE IF EXISTS query_student;
CREATE PROCEDURE query_student()
BEGIN
    #     SELECT * FROM student;
    SELECT IFNULL(student.student_id, '')                                          AS '学号',
           IFNULL(student.identity_card_number, '')                                AS '身份证号',
           IFNULL(student.name, '')                                                AS '姓名',
           IFNULL(IF(student.gender = 'M', '男', '女'), '')                          AS '性别',
           IFNULL(province_or_municipality.province_or_municipality_full_name, '') AS '省份/直辖市名',
           IFNULL(student.detailed_address, '')                                    AS '详细地址',
           IFNULL(student.phone_number, '')                                        AS '手机号',
           IFNULL(student.mailbox, '')                                             AS '邮箱',
           IFNULL(poor_degree.poor_degree_name, '')                                AS '贫困程度名',
           IFNULL(student.school, '')                                              AS '学院',
           IFNULL(student.department, '')                                          AS '系',
           IFNULL(student.major, '')                                               AS '专业',
           IFNULL(IF(student.is_diagnosed = 'Y', '是', '否'), '')                     AS '是否已确诊',
           IFNULL(student.comment, '')                                             AS '备注'
    FROM student
             LEFT OUTER JOIN province_or_municipality
                             ON student.province_or_municipality_id =
                                province_or_municipality.province_or_municipality_id
             LEFT OUTER JOIN poor_degree
                             ON student.poor_degree_id = poor_degree.poor_degree_id;
END;
# CALL query_student();

# SELECT student # by student_id
DROP PROCEDURE IF EXISTS query_student_by_student_id;
CREATE PROCEDURE query_student_by_student_id(
    IN student_id CHAR(11)
)
BEGIN
    #     SELECT *
#     FROM student
#     WHERE student.student_id = student_id;
    SELECT IFNULL(student.student_id, '')                                          AS '学号',
           IFNULL(student.identity_card_number, '')                                AS '身份证号',
           IFNULL(student.name, '')                                                AS '姓名',
           IFNULL(IF(student.gender = 'M', '男', '女'), '')                          AS '性别',
           IFNULL(province_or_municipality.province_or_municipality_full_name, '') AS '省份/直辖市名',
           IFNULL(student.detailed_address, '')                                    AS '详细地址',
           IFNULL(student.phone_number, '')                                        AS '手机号',
           IFNULL(student.mailbox, '')                                             AS '邮箱',
           IFNULL(poor_degree.poor_degree_name, '')                                AS '贫困程度名',
           IFNULL(student.school, '')                                              AS '学院',
           IFNULL(student.department, '')                                          AS '系',
           IFNULL(student.major, '')                                               AS '专业',
           IFNULL(IF(student.is_diagnosed = 'Y', '是', '否'), '')                     AS '是否已确诊',
           IFNULL(student.comment, '')                                             AS '备注'
    FROM student
             LEFT OUTER JOIN province_or_municipality
                             ON student.province_or_municipality_id =
                                province_or_municipality.province_or_municipality_id
             LEFT OUTER JOIN poor_degree
                             ON student.poor_degree_id = poor_degree.poor_degree_id
    WHERE student.student_id = student_id;
END;
# CALL query_student_by_student_id('20172002000');

# INSERT student
DROP PROCEDURE IF EXISTS insert_into_student;
CREATE PROCEDURE insert_into_student(
    IN student_id CHAR(11),
    IN identity_card_number CHAR(18),
    IN name VARCHAR(40),
    IN gender CHAR(1),
    IN province_or_municipality_full_name VARCHAR(10),
    IN detailed_address TEXT,
    IN phone_number TEXT,
    IN mailbox TEXT,
    IN poor_degree_id VARCHAR(10),
    IN school VARCHAR(15),
    IN department VARCHAR(15),
    IN major VARCHAR(15),
    IN is_diagnosed CHAR(1),
    IN comment LONGTEXT,
    IN password VARCHAR(20)
)
BEGIN
    DECLARE province_or_municipality_id CHAR(2);
    DECLARE poor_degree_id CHAR(1);

    SELECT province_or_municipality.province_or_municipality_id
    INTO province_or_municipality_id
    FROM province_or_municipality
    WHERE province_or_municipality.province_or_municipality_full_name = province_or_municipality_full_name;

    INSERT INTO student(student_id, identity_card_number, name, gender, province_or_municipality_id, detailed_address,
                        phone_number, mailbox, poor_degree_id, school, department, major, is_diagnosed, comment,
                        password)
    VALUES (student_id, identity_card_number, name, gender, province_or_municipality_id, detailed_address, phone_number,
            mailbox, poor_degree_id, school, department, major, is_diagnosed, comment, password);
END;
# CALL insert_into_student('20172002002', '32130220001010003X', '李馨', 'F', '01', '江苏省南京市浦口区新城小区3栋2单元202室', '13100001003',
#                          'xl@mail.example.com', '0', '人工智能学院', '人工智能系', '人工智能', 'Y', NULL, 'fsfwdo32jw1');

# UPDATE student # for_super_admin
DROP PROCEDURE IF EXISTS update_student_for_super_admin;
CREATE PROCEDURE update_student_for_super_admin(
    IN student_id CHAR(11),
    IN identity_card_number CHAR(18),
    IN name VARCHAR(40),
    IN gender CHAR(1),
    IN province_or_municipality_full_name VARCHAR(10),
    IN detailed_address TEXT,
    IN phone_number TEXT,
    IN mailbox TEXT,
    IN poor_degree_name VARCHAR(10),
    IN school VARCHAR(15),
    IN department VARCHAR(15),
    IN major VARCHAR(15),
    IN is_diagnosed CHAR(1),
    IN comment LONGTEXT
#     IN password VARCHAR(20)
)
BEGIN
    DECLARE province_or_municipality_id CHAR(2);
    DECLARE poor_degree_id CHAR(1);

    SELECT province_or_municipality.province_or_municipality_id
    INTO province_or_municipality_id
    FROM province_or_municipality
    WHERE province_or_municipality.province_or_municipality_full_name = province_or_municipality_full_name;

    SELECT poor_degree.poor_degree_id
    INTO poor_degree_id
    FROM poor_degree
    WHERE poor_degree.poor_degree_name = poor_degree_name;

    UPDATE student
    SET
#         student.student_id                  = '20172002002',
student.identity_card_number        = identity_card_number,
student.name                        = name,
student.gender                      = gender,
student.province_or_municipality_id = province_or_municipality_id,
student.detailed_address            = detailed_address,
student.phone_number                = phone_number,
student.mailbox                     = mailbox,
student.poor_degree_id              = poor_degree_id,
student.school                      = school,
student.department                  = department,
student.major                       = major,
student.is_diagnosed                 = is_diagnosed,
student.comment                     = comment
# student.password                    = password
    WHERE student.student_id = student_id;
END;
# CALL update_student_for_super_admin('20172002002', '32130220001010003X', '李馨', 'F', '江苏省', '江苏省南京市浦口区新城小区3栋2单元202室', '13100001003',
#                     'xl@mail.example.com', '非贫困', '人工智能学院', '人工智能系', '人工智能', 'Y', '阴霾总会过去！', 'fsfwdo32jw1');

# UPDATE student # update_student_for_student
DROP PROCEDURE IF EXISTS update_student_for_student;
CREATE PROCEDURE update_student_for_student(
    IN student_id CHAR(11),
    IN identity_card_number CHAR(18),
    IN name VARCHAR(40),
    IN gender CHAR(1),
    IN province_or_municipality_full_name VARCHAR(10),
    IN detailed_address TEXT,
    IN phone_number TEXT,
    IN mailbox TEXT,
    #     IN poor_degree_name VARCHAR(10),
#     IN school VARCHAR(15),
#     IN department VARCHAR(15),
#     IN major VARCHAR(15),
    IN is_diagnosed CHAR(1),
    IN comment LONGTEXT
#     IN password VARCHAR(20)
)
BEGIN
    DECLARE province_or_municipality_id CHAR(2);
#     DECLARE poor_degree_id CHAR(1);

    SELECT province_or_municipality.province_or_municipality_id
    INTO province_or_municipality_id
    FROM province_or_municipality
    WHERE province_or_municipality.province_or_municipality_full_name = province_or_municipality_full_name;

    #     SELECT poor_degree.poor_degree_id
#     INTO poor_degree_id
#     FROM poor_degree
#     WHERE poor_degree.poor_degree_name = poor_degree_name;

    UPDATE student
    SET
#         student.student_id                  = '20172002002',
student.identity_card_number        = identity_card_number,
student.name                        = name,
student.gender                      = gender,
student.province_or_municipality_id = province_or_municipality_id,
student.detailed_address            = detailed_address,
student.phone_number                = phone_number,
student.mailbox                     = mailbox,
# student.poor_degree_id              = poor_degree_id,
# student.school                      = school,
# student.department                  = department,
# student.major                       = major,
student.is_diagnosed                 = is_diagnosed,
student.comment                     = comment
# student.password                    = password
    WHERE student.student_id = student_id;
END;
# CALL update_student_for_student('20172002002', '32130220001010003X', '李馨', 'F', '江苏省', '江苏省南京市浦口区新城小区3栋2单元202室', '13100001003',
#                     'xl@mail.example.com', 'Y', '阴霾总会过去！');

# DELETE student
DROP PROCEDURE IF EXISTS delete_student;
CREATE PROCEDURE delete_student(
    IN student_id CHAR(11)
)
BEGIN
    DELETE
    FROM student
    WHERE student.student_id = student_id;
END;
# CALL delete_student('20172002002');