# SELECT # object structures
SELECT TABLE_NAME                          AS '表名',
       ORDINAL_POSITION                    AS '序号',
       COLUMN_NAME                         AS '字段名称',
       COLUMN_COMMENT                      AS '字段中文名',
       COLUMN_TYPE                         AS '字段类型',
       IF(COLUMN_KEY = 'PRI', 'YES', 'NO') AS '是否为主键',
       IF(COLUMN_KEY = 'UNI', 'YES', 'NO') AS '是否唯一',
       IS_NULLABLE                         AS '能否为空'
FROM information_schema.COLUMNS
WHERE TABLE_SCHEMA = 'aepmis'
  AND TABLE_NAME IN (SELECT DISTINCT TABLE_NAME
                     FROM information_schema.COLUMNS_EXTENSIONS
                     WHERE TABLE_SCHEMA = 'aepmis')
ORDER BY TABLE_NAME, ORDINAL_POSITION;
# INTO OUTFILE 'C:/Users/Administrator/Desktop/account.xls';
# INTO OUTFILE 'C:/Users/spwxg/Desktop/table/object-structures.xlsx';


