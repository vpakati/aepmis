# d-列名
# d-IFNULL
USE aepmis;

# ---------------------------
# SELECT daily_health_report # today # 今日已确诊
DROP PROCEDURE IF EXISTS query_those_who_is_diagnosed_today;
CREATE PROCEDURE query_those_who_is_diagnosed_today()
BEGIN
    SELECT '学生'                                                                        AS '身份',
           IFNULL(daily_health_report.signer_id, '')                                   AS '学号/教职工号',
           IFNULL(student.name, '')                                                    AS '姓名',
           IFNULL(daily_health_report.time_of_sign, '')                                AS '填写时间',
           IFNULL(daily_health_report.temperature, '')                                 AS '体温',
           IFNULL(daily_health_report.cur_position, '')                                AS '当时位置',
           IFNULL(IF(daily_health_report.have_been_to_risk_areas = 'Y', '是', '否'), '') AS '是否前往过风险地区',
           IFNULL(IF(daily_health_report.have_contacted = 'Y', '是', '否'), '')          AS '是否接触过确诊/疑似者',
           IFNULL(IF(daily_health_report.is_diagnosed = 'Y', '是', '否'), '')            AS '是否已确诊',
           IFNULL(daily_health_report.comment, '')                                     AS '备注'
    FROM daily_health_report
             INNER JOIN student
                        ON daily_health_report.signer_id = student.student_id
    WHERE (daily_health_report.is_diagnosed = 'Y')
      AND DATE(daily_health_report.time_of_sign) = CURDATE()
    UNION
    SELECT '教职工' AS '身份',
           IFNULL(daily_health_report.signer_id, ''),
           IFNULL(staff.name, ''),
           IFNULL(daily_health_report.time_of_sign, ''),
           IFNULL(daily_health_report.temperature, ''),
           IFNULL(daily_health_report.cur_position, ''),
           IFNULL(IF(daily_health_report.have_been_to_risk_areas = 'Y', '是', '否'), ''),
           IFNULL(IF(daily_health_report.have_contacted = 'Y', '是', '否'), ''),
           IFNULL(IF(daily_health_report.is_diagnosed = 'Y', '是', '否'), ''),
           IFNULL(daily_health_report.comment, '')
    FROM daily_health_report
             INNER JOIN staff
                        ON daily_health_report.signer_id = staff.staff_id
    WHERE (daily_health_report.is_diagnosed = 'Y')
      AND DATE(daily_health_report.time_of_sign) = CURDATE();
END;
# CALL query_those_who_is_diagnosed_today();

# SELECT daily_health_report # today # 今日疑似确诊
DROP PROCEDURE IF EXISTS query_those_who_might_be_diagnosed_today;
CREATE PROCEDURE query_those_who_might_be_diagnosed_today()
BEGIN
    SELECT '学生'                                                                        AS '身份',
           IFNULL(daily_health_report.signer_id, '')                                   AS '学号/教职工号',
           IFNULL(student.name, '')                                                    AS '姓名',
           IFNULL(daily_health_report.time_of_sign, '')                                AS '填写时间',
           IFNULL(daily_health_report.temperature, '')                                 AS '体温',
           IFNULL(daily_health_report.cur_position, '')                                AS '当时位置',
           IFNULL(IF(daily_health_report.have_been_to_risk_areas = 'Y', '是', '否'), '') AS '是否前往过风险地区',
           IFNULL(IF(daily_health_report.have_contacted = 'Y', '是', '否'), '')          AS '是否接触过确诊/疑似者',
           IFNULL(IF(daily_health_report.is_diagnosed = 'Y', '是', '否'), '')            AS '是否已确诊',
           IFNULL(daily_health_report.comment, '')                                     AS '备注'
    FROM daily_health_report
             INNER JOIN student
                        ON daily_health_report.signer_id = student.student_id
    WHERE (daily_health_report.temperature > 37.7
        OR daily_health_report.have_been_to_risk_areas = 'Y'
        OR daily_health_report.have_contacted = 'Y')
      AND daily_health_report.is_diagnosed = 'U'
      AND DATE(daily_health_report.time_of_sign) = CURDATE()
    UNION
    SELECT '教职工',
           IFNULL(daily_health_report.signer_id, ''),
           IFNULL(staff.name, ''),
           IFNULL(daily_health_report.time_of_sign, ''),
           IFNULL(daily_health_report.temperature, ''),
           IFNULL(daily_health_report.cur_position, ''),
           IFNULL(IF(daily_health_report.have_been_to_risk_areas = 'Y', '是', '否'), ''),
           IFNULL(IF(daily_health_report.have_contacted = 'Y', '是', '否'), ''),
           IFNULL(IF(daily_health_report.is_diagnosed = 'Y', '是', '否'), ''),
           IFNULL(daily_health_report.comment, '')
    FROM daily_health_report
             INNER JOIN staff
                        ON daily_health_report.signer_id = staff.staff_id
    WHERE (daily_health_report.temperature > 37.7
        OR daily_health_report.have_been_to_risk_areas = 'Y'
        OR daily_health_report.have_contacted = 'Y')
      AND daily_health_report.is_diagnosed = 'U'
      AND DATE(daily_health_report.time_of_sign) = CURDATE();
END;
# CALL query_those_who_might_be_diagnosed_today();
# ---------------------------

# SELECT daily_health_report # 今日尚未填报的用户
DROP PROCEDURE IF EXISTS query_those_who_have_not_report_today;
CREATE PROCEDURE query_those_who_have_not_report_today()
BEGIN
    #     SELECT DISTINCT '学生'                                                AS '身份',
    SELECT '学生'                                                 AS '身份',
           IFNULL(student.student_id, '')                       AS '学号/教职工号',
           IFNULL(student.name, '')                             AS '姓名',
           IFNULL(student.phone_number, '')                     AS '手机号',
           IFNULL(student.mailbox, '')                          AS '邮箱',
           IFNULL(IF(student.is_diagnosed = 'Y', '是', '否'), '') AS '是否已确诊'
    FROM student
             LEFT OUTER JOIN daily_health_report
                             ON student.student_id = daily_health_report.signer_id
    WHERE student.student_id NOT IN (
#         SELECT DISTINCT student.student_id
        SELECT student.student_id
        FROM student
                 LEFT OUTER JOIN daily_health_report
                                 ON student.student_id = daily_health_report.signer_id
        WHERE DATE(daily_health_report.time_of_sign) = CURDATE()
    )
    UNION
#     SELECT DISTINCT '教职工',
    SELECT '教职工',
           IFNULL(staff.staff_id, ''),
           IFNULL(staff.name, ''),
           IFNULL(staff.phone_number, ''),
           IFNULL(staff.mailbox, ''),
           IFNULL(IF(staff.is_diagnosed = 'Y', '是', '否'), '')
    FROM staff
             LEFT OUTER JOIN daily_health_report
                             ON staff.staff_id = daily_health_report.signer_id
    WHERE staff.staff_id NOT IN (
#         SELECT DISTINCT staff.staff_id
        SELECT staff.staff_id
        FROM staff
                 LEFT OUTER JOIN daily_health_report
                                 ON staff.staff_id = daily_health_report.signer_id
        WHERE DATE(daily_health_report.time_of_sign) = CURDATE()
    );
END;
# CALL query_those_who_have_not_report_today();

# SELECT daily_health_report # for_admin_or_above
DROP PROCEDURE IF EXISTS query_daily_health_report_for_admin_or_above;
CREATE PROCEDURE query_daily_health_report_for_admin_or_above()
BEGIN
    SELECT '学生'                                                                        AS '身份',
           IFNULL(daily_health_report.signer_id, '')                                   AS '学号/教职工号',
           IFNULL(student.name, '')                                                    AS '姓名',
           IFNULL(daily_health_report.time_of_sign, '')                                AS '填写时间',
           IFNULL(daily_health_report.temperature, '')                                 AS '体温',
           IFNULL(daily_health_report.cur_position, '')                                AS '当时位置',
           IFNULL(IF(daily_health_report.have_been_to_risk_areas = 'Y', '是', '否'), '') AS '是否前往过风险地区',
           IFNULL(IF(daily_health_report.have_contacted = 'Y', '是', '否'), '')          AS '是否接触过确诊/疑似者',
           IFNULL(IF(daily_health_report.is_diagnosed = 'Y', '是', '否'), '')            AS '是否已确诊',
           IFNULL(daily_health_report.comment, '')                                     AS '备注'
    FROM daily_health_report
             INNER JOIN student
                        ON daily_health_report.signer_id = student.student_id
    UNION
    SELECT '教职工',
           IFNULL(daily_health_report.signer_id, ''),
           IFNULL(staff.name, ''),
           IFNULL(daily_health_report.time_of_sign, ''),
           IFNULL(daily_health_report.temperature, ''),
           IFNULL(daily_health_report.cur_position, ''),
           IFNULL(IF(daily_health_report.have_been_to_risk_areas = 'Y', '是', '否'), ''),
           IFNULL(IF(daily_health_report.have_contacted = 'Y', '是', '否'), ''),
           IFNULL(IF(daily_health_report.is_diagnosed = 'Y', '是', '否'), ''),
           IFNULL(daily_health_report.comment, '')
    FROM daily_health_report
             INNER JOIN staff
                        ON daily_health_report.signer_id = staff.staff_id;
END;
# CALL query_daily_health_report_for_admin_or_above();

# SELECT daily_health_report # for_admin_or_above # today
DROP PROCEDURE IF EXISTS query_daily_health_report_of_today_for_admin_or_above;
CREATE PROCEDURE query_daily_health_report_of_today_for_admin_or_above()
BEGIN
    SELECT '学生'                                                                        AS '身份',
           IFNULL(daily_health_report.signer_id, '')                                   AS '学号/教职工号',
           IFNULL(student.name, '')                                                    AS '姓名',
           IFNULL(daily_health_report.time_of_sign, '')                                AS '填写时间',
           IFNULL(daily_health_report.temperature, '')                                 AS '体温',
           IFNULL(daily_health_report.cur_position, '')                                AS '当时位置',
           IFNULL(IF(daily_health_report.have_been_to_risk_areas = 'Y', '是', '否'), '') AS '是否前往过风险地区',
           IFNULL(IF(daily_health_report.have_contacted = 'Y', '是', '否'), '')          AS '是否接触过确诊/疑似者',
           IFNULL(IF(daily_health_report.is_diagnosed = 'Y', '是', '否'), '')            AS '是否已确诊',
           IFNULL(daily_health_report.comment, '')                                     AS '备注'
    FROM daily_health_report
             INNER JOIN student
                        ON daily_health_report.signer_id = student.student_id
    WHERE DATE(daily_health_report.time_of_sign) = CURDATE()
    UNION
    SELECT '教职工',
           IFNULL(daily_health_report.signer_id, ''),
           IFNULL(staff.name, ''),
           IFNULL(daily_health_report.time_of_sign, ''),
           IFNULL(daily_health_report.temperature, ''),
           IFNULL(daily_health_report.cur_position, ''),
           IFNULL(IF(daily_health_report.have_been_to_risk_areas = 'Y', '是', '否'), ''),
           IFNULL(IF(daily_health_report.have_contacted = 'Y', '是', '否'), ''),
           IFNULL(IF(daily_health_report.is_diagnosed = 'Y', '是', '否'), ''),
           IFNULL(daily_health_report.comment, '')
    FROM daily_health_report
             INNER JOIN staff
                        ON daily_health_report.signer_id = staff.staff_id
    WHERE DATE(daily_health_report.time_of_sign) = CURDATE();
END;
# CALL query_daily_health_report_of_today();

# SELECT daily_health_report # for_common_user
DROP PROCEDURE IF EXISTS query_daily_health_report_for_common_user;
CREATE PROCEDURE query_daily_health_report_for_common_user(IN user_id VARCHAR(11))
BEGIN
    SELECT IFNULL(daily_health_report.time_of_sign, '')                                AS '填写时间',
           IFNULL(daily_health_report.temperature, '')                                 AS '体温',
           IFNULL(daily_health_report.cur_position, '')                                AS '当时位置',
           IFNULL(IF(daily_health_report.have_been_to_risk_areas = 'Y', '是', '否'), '') AS '是否前往过风险地区',
           IFNULL(IF(daily_health_report.have_contacted = 'Y', '是', '否'), '')          AS '是否接触过确诊/疑似者',
           IFNULL(IF(daily_health_report.is_diagnosed = 'Y', '是', '否'), '')            AS '是否已确诊',
           IFNULL(daily_health_report.comment, '')                                     AS '备注'
    FROM daily_health_report
    WHERE daily_health_report.signer_id = user_id;
END;
# CALL query_daily_health_report_for_common_user('20172002000');

# SELECT daily_health_report # for_common_user # taday
DROP PROCEDURE IF EXISTS query_daily_health_report_of_today_for_common_user;
CREATE PROCEDURE query_daily_health_report_of_today_for_common_user(IN user_id VARCHAR(11))
BEGIN
    SELECT IFNULL(daily_health_report.time_of_sign, '')                                AS '填写时间',
           IFNULL(daily_health_report.temperature, '')                                 AS '体温',
           IFNULL(daily_health_report.cur_position, '')                                AS '当时位置',
           IFNULL(IF(daily_health_report.have_been_to_risk_areas = 'Y', '是', '否'), '') AS '是否前往过风险地区',
           IFNULL(IF(daily_health_report.have_contacted = 'Y', '是', '否'), '')          AS '是否接触过确诊/疑似者',
           IFNULL(IF(daily_health_report.is_diagnosed = 'Y', '是', '否'), '')            AS '是否已确诊',
           IFNULL(daily_health_report.comment, '')                                     AS '备注'
    FROM daily_health_report
    WHERE daily_health_report.signer_id = user_id
      AND DATE(daily_health_report.time_of_sign) = CURDATE();
END;
# CALL query_daily_health_report_of_today_for_common_user('20172002000');

# INSERT daily_health_report
DROP PROCEDURE IF EXISTS insert_into_daily_health_report;
CREATE PROCEDURE insert_into_daily_health_report(
    IN signer_id VARCHAR(11),
    IN temperature DOUBLE(3, 1),
    IN cur_position TEXT,
    IN have_been_to_risk_areas CHAR(1),
    IN have_contacted CHAR(1),
    IN is_diagnosed CHAR(1),
    IN comment LONGTEXT,
    IN user_type VARCHAR(11)
)
BEGIN
    INSERT INTO daily_health_report (signer_id,
                                     time_of_sign,
                                     temperature,
                                     cur_position,
                                     have_been_to_risk_areas,
                                     have_contacted,
                                     is_diagnosed,
                                     comment)
    VALUES (signer_id, NOW(), temperature, cur_position, have_been_to_risk_areas, have_contacted, is_diagnosed,
            comment);

    IF @@error_count = 0
    THEN
        IF user_type = '学生'
        THEN
            IF is_diagnosed = 'Y'
            THEN
                UPDATE student
                SET is_diagnosed = 'Y'
                WHERE student_id = signer_id;
            ELSEIF is_diagnosed = 'N'
            THEN
                UPDATE student
                SET is_diagnosed = 'N'
                WHERE student_id = signer_id;
            END IF;
        ELSEIF user_type = '教职工'
        THEN
            IF is_diagnosed = 'Y'
            THEN
                UPDATE staff
                SET is_diagnosed = 'Y'
                WHERE staff_id = signer_id;
            ELSEIF is_diagnosed = 'N'
            THEN
                UPDATE staff
                SET is_diagnosed = 'N'
                WHERE staff_id = signer_id;
            END IF;
        END IF;
    END IF;
END;
# CALL insert_into_daily_health_report('20172002000', 36.3, '江苏省南京市宁六路219号南京信息工程大学', 'Y', 'Y', 'Y', '已隔离', '学生');

# UPDATE daily_health_report
# 暂不加入修改健康日报记录的功能

# DELETE daily_health_report
# 暂不加入删除健康日报记录的功能