# d-列名
# d-IFNULL
USE aepmis;

# SELECT staff
DROP PROCEDURE IF EXISTS query_staff;
CREATE PROCEDURE query_staff()
BEGIN
    SELECT IFNULL(staff.staff_id, '')                                              AS '教职工号',
           IFNULL(staff.identity_card_number, '')                                  AS '身份证号',
           IFNULL(staff.name, '')                                                  AS '姓名',
           IFNULL(staff_category.staff_category_name, '')                          AS '教职工类别名',
           IFNULL(IF(staff.gender = 'M', '男', '女'), '')                            AS '性别',
           IFNULL(province_or_municipality.province_or_municipality_full_name, '') AS '省份/直辖市名',
           IFNULL(staff.detailed_address, '')                                      AS '详细地址',
           IFNULL(poor_degree.poor_degree_name, '')                                AS '贫困程度名',
           IFNULL(staff.mailbox, '')                                               AS '邮箱',
           IFNULL(staff.phone_number, '')                                          AS '手机号',
           IFNULL(staff.comment, '')                                               AS '备注',
           IFNULL(IF(staff.is_diagnosed = 'Y', '是', '否'), '')                       AS '是否已确诊'
    FROM staff
             LEFT OUTER JOIN staff_category
                        ON staff.staff_category_id = staff_category.staff_category_id
             LEFT OUTER JOIN province_or_municipality
                        ON staff.province_or_municipality_id = province_or_municipality.province_or_municipality_id
             LEFT OUTER JOIN poor_degree
                        ON staff.poor_degree_id = poor_degree.poor_degree_id;
END;
# CALL query_staff();

# SELECT staff # by_staff_id
DROP PROCEDURE IF EXISTS query_staff_by_staff_id;
CREATE PROCEDURE query_staff_by_staff_id(IN staff_id CHAR(5))
BEGIN
    SELECT IFNULL(staff.staff_id, '')                                              AS '教职工号',
           IFNULL(staff.identity_card_number, '')                                  AS '身份证号',
           IFNULL(staff.name, '')                                                  AS '姓名',
           IFNULL(staff_category.staff_category_name, '')                          AS '教职工类别名',
           IFNULL(IF(staff.gender = 'M', '男', '女'), '')                            AS '性别',
           IFNULL(province_or_municipality.province_or_municipality_full_name, '') AS '省份/直辖市名',
           IFNULL(staff.detailed_address, '')                                      AS '详细地址',
           IFNULL(poor_degree.poor_degree_name, '')                                AS '贫困程度名',
           IFNULL(staff.mailbox, '')                                               AS '邮箱',
           IFNULL(staff.phone_number, '')                                          AS '手机号',
           IFNULL(staff.comment, '')                                               AS '备注',
           IFNULL(IF(staff.is_diagnosed = 'Y', '是', '否'), '')                       AS '是否已确诊'
    FROM staff
             LEFT OUTER JOIN staff_category
                        ON staff.staff_category_id = staff_category.staff_category_id
             LEFT OUTER JOIN province_or_municipality
                        ON staff.province_or_municipality_id = province_or_municipality.province_or_municipality_id
             LEFT OUTER JOIN poor_degree
                        ON staff.poor_degree_id = poor_degree.poor_degree_id
    WHERE staff.staff_id = staff_id;
END;
# CALL query_staff_by_staff_id('00100');

# INSERT staff
DROP PROCEDURE IF EXISTS insert_into_staff;
CREATE PROCEDURE insert_into_staff(
    IN staff_id CHAR(5),
    IN identity_card_number CHAR(18),
    IN name VARCHAR(40),
    IN staff_category_name VARCHAR(15),
    IN gender VARCHAR(1),
    IN province_or_municipality_full_name VARCHAR(10),
    IN detailed_address TEXT,
    IN poor_degree_name VARCHAR(10),
    IN mailbox TEXT,
    IN phone_number TEXT,
    IN password VARCHAR(20),
    IN comment LONGTEXT,
    IN is_diagnosed CHAR(1)
)
BEGIN
    DECLARE staff_category_id CHAR(2);
    DECLARE province_or_municipality_id CHAR(2);
    DECLARE poor_degree_id CHAR(1);

    SELECT staff_category.staff_category_id
    INTO staff_category_id
    FROM staff_category
    WHERE staff_category.staff_category_name = staff_category_name;

    SELECT province_or_municipality.province_or_municipality_id
    INTO province_or_municipality_id
    FROM province_or_municipality
    WHERE province_or_municipality.province_or_municipality_full_name = province_or_municipality_full_name;

    SELECT poor_degree.poor_degree_id
    INTO poor_degree_id
    FROM poor_degree
    WHERE poor_degree.poor_degree_name = poor_degree_name;

    INSERT INTO staff(staff_id, identity_card_number, name, staff_category_id, gender, province_or_municipality_id,
                      detailed_address, poor_degree_id, mailbox, phone_number, password, comment, is_diagnosed)
    VALUES (staff_id, identity_card_number, name, staff_category_id, gender, province_or_municipality_id,
            detailed_address, poor_degree_id, mailbox, phone_number, password, comment, is_diagnosed);
END;
# CALL insert_into_staff('00104', '32130219901010003X', '李凯', '行政人员', 'M', '上海市', '上海市徐汇区梓树园3栋1单元904室', '非贫困',
#                        'kl@mail.example.com', '13100001002', 'fsdfbfls4', '', 'N');

# UPDATE staff # for_staff
DROP PROCEDURE IF EXISTS update_staff_for_staff;
CREATE PROCEDURE update_staff_for_staff(
    IN staff_id CHAR(5),
    IN identity_card_number CHAR(18),
    IN name VARCHAR(40),
#     IN staff_category_name VARCHAR(15),
    IN gender VARCHAR(1),
    IN province_or_municipality_full_name VARCHAR(10),
    IN detailed_address TEXT,
#     IN poor_degree_name VARCHAR(10),
    IN mailbox TEXT,
    IN phone_number TEXT,
#     IN password VARCHAR(20),
    IN comment LONGTEXT,
    IN is_diagnosed CHAR(1)
)
BEGIN
    #     DECLARE staff_category_id CHAR(2);
    DECLARE province_or_municipality_id CHAR(2);
    #     DECLARE poor_degree_id CHAR(1);

    #     SELECT staff_category.staff_category_id
#     INTO staff_category_id
#     FROM staff_category
#     WHERE staff_category.staff_category_name = staff_category_name;

    SELECT province_or_municipality.province_or_municipality_id
    INTO province_or_municipality_id
    FROM province_or_municipality
    WHERE province_or_municipality.province_or_municipality_full_name = province_or_municipality_full_name;

    #     SELECT poor_degree.poor_degree_id
#     INTO poor_degree_id
#     FROM poor_degree
#     WHERE poor_degree.poor_degree_name = poor_degree_name;

    UPDATE staff
    SET
#         staff.staff_id                    = '00104',
staff.identity_card_number        = identity_card_number,
staff.name                        = name,
#         staff.staff_category_id           = '行政人员',
staff.gender                      = gender,
staff.province_or_municipality_id = province_or_municipality_id,
staff.detailed_address            = detailed_address,
# staff.poor_degree_id              = poor_degree_id,
staff.mailbox                     = mailbox,
staff.phone_number                = phone_number,
# staff.password                    = password,
staff.comment                     = comment,
staff.is_diagnosed                 = is_diagnosed
    WHERE staff.staff_id = staff_id;
END;
# CALL update_staff_for_staff('00104', '32130219901010003X', '李凯', 'M', '上海市', '上海市徐汇区梓树园3栋1单元904室',
#                             'kl@mail.example.com', '13100001002', '1111111', '+++', 'N');

# UPDATE staff # for_super_admin
DROP PROCEDURE IF EXISTS update_staff_for_super_admin;
CREATE PROCEDURE update_staff_for_super_admin(
    IN staff_id CHAR(5),
    IN identity_card_number CHAR(18),
    IN name VARCHAR(40),
    IN staff_category_name VARCHAR(15),
    IN gender VARCHAR(1),
    IN province_or_municipality_full_name VARCHAR(10),
    IN detailed_address TEXT,
    IN poor_degree_name VARCHAR(10),
    IN mailbox TEXT,
    IN phone_number TEXT,
#     IN password VARCHAR(20),
    IN comment LONGTEXT,
    IN is_diagnosed CHAR(1)
)
BEGIN
    DECLARE staff_category_id CHAR(2);
    DECLARE province_or_municipality_id CHAR(2);
    DECLARE poor_degree_id CHAR(1);

    SELECT staff_category.staff_category_id
    INTO staff_category_id
    FROM staff_category
    WHERE staff_category.staff_category_name = staff_category_name;

    SELECT province_or_municipality.province_or_municipality_id
    INTO province_or_municipality_id
    FROM province_or_municipality
    WHERE province_or_municipality.province_or_municipality_full_name = province_or_municipality_full_name;

    SELECT poor_degree.poor_degree_id
    INTO poor_degree_id
    FROM poor_degree
    WHERE poor_degree.poor_degree_name = poor_degree_name;

    UPDATE staff
    SET
#         staff.staff_id                    = staff_id,
staff.identity_card_number        = identity_card_number,
staff.name                        = name,
staff.staff_category_id           = staff_category_id,
staff.gender                      = gender,
staff.province_or_municipality_id = province_or_municipality_id,
staff.detailed_address            = detailed_address,
staff.poor_degree_id              = poor_degree_id,
staff.mailbox                     = mailbox,
staff.phone_number                = phone_number,
# staff.password                    = password,
staff.comment                     = comment,
staff.is_diagnosed                 = is_diagnosed
    WHERE staff.staff_id = staff_id;
END;
# CALL update_staff_for_super_admin('00104', '32130219901010003X', '李凯', '行政人员', 'M', '上海市', '上海市徐汇区梓树园3栋1单元904室', '非贫困',
#                                       'kl@mail.example.com', '13100001002', '1111111', '+++', 'N');

# DELETE staff
DROP PROCEDURE IF EXISTS delete_staff;
CREATE PROCEDURE delete_staff(
    IN staff_id CHAR(5)
)
BEGIN
    DELETE
    FROM staff
    WHERE staff.staff_id = staff_id;
END;
# CALL delete_staff('00104');