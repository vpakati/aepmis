# d-列名
# d-IFNULL
USE aepmis;

# SELECT permission_assignment # by_staff_id
DROP PROCEDURE IF EXISTS query_responsibility_assignment_by_staff_id;
CREATE PROCEDURE query_responsibility_assignment_by_staff_id(IN staff_id CHAR(5))
BEGIN
    SELECT IFNULL(staff.name, '')                          AS '姓名',
           IFNULL(responsibility.responsibility_name, '无') AS '（防疫）职责名',
           IFNULL(responsibility_assignment.comment, '')   AS '备注'
    FROM responsibility_assignment
             INNER JOIN responsibility
                        ON responsibility_assignment.responsibility_id = responsibility.responsibility_id
             RIGHT OUTER JOIN staff
                              ON responsibility_assignment.staff_id = staff.staff_id
    WHERE staff.staff_id = staff_id;
END;
# CALL query_responsibility_assignment_by_staff_id('00102');


# SELECT responsibility_assignment
DROP PROCEDURE IF EXISTS query_responsibility_assignment;
CREATE PROCEDURE query_responsibility_assignment()
BEGIN
    SELECT IFNULL(staff.name, '')                          AS '姓名',
           IFNULL(responsibility.responsibility_name, '无') AS '（防疫）职责名',
           IFNULL(responsibility_assignment.comment, '')   AS '备注'
    FROM responsibility_assignment
             INNER JOIN responsibility
                        ON responsibility_assignment.responsibility_id = responsibility.responsibility_id
             RIGHT OUTER JOIN staff
                              ON responsibility_assignment.staff_id = staff.staff_id;
END;
# CALL query_responsibility_assignment();

# INSERT responsibility_assignment
DROP PROCEDURE IF EXISTS insert_into_responsibility_assignment;
CREATE PROCEDURE insert_into_responsibility_assignment(
    IN staff_id CHAR(5),
    IN responsibility_name VARCHAR(10),
    IN comment LONGTEXT)
BEGIN
    DECLARE responsibility_id CHAR(2);
    SELECT responsibility.responsibility_id
    INTO responsibility_id
    FROM responsibility
    WHERE responsibility.responsibility_name = responsibility_name;

    INSERT INTO responsibility_assignment(staff_id, responsibility_id, comment)
    VALUES (staff_id, responsibility_id, comment);
END;
# CALL insert_into_responsibility_assignment('00103', '专业级负责人', '');


# UPDATE responsibility_assignment
DROP PROCEDURE IF EXISTS update_responsibility_assignment;
CREATE PROCEDURE update_responsibility_assignment(
    IN staff_id CHAR(5),
    IN responsibility_name VARCHAR(15),
    IN comment LONGTEXT
)
BEGIN
    DECLARE responsibility_id CHAR(2);

    SELECT responsibility.responsibility_id
    INTO responsibility_id
    FROM responsibility
    WHERE responsibility.responsibility_name = responsibility_name;

    UPDATE responsibility_assignment
    SET responsibility_assignment.responsibility_id = responsibility_id,
        responsibility_assignment.comment           = comment
    WHERE responsibility_assignment.staff_id = staff_id;
END;
# CALL update_responsibility_assignment('00103', '专业级负责人', '');

# DELETE responsibility_assignment
DROP PROCEDURE IF EXISTS delete_responsibility_assignment;
CREATE PROCEDURE delete_responsibility_assignment(IN staff_id CHAR(5))
BEGIN
    DELETE
    FROM responsibility_assignment
    WHERE responsibility_assignment.staff_id = staff_id;
END;
# CALL delete_responsibility_assignment('00103');
