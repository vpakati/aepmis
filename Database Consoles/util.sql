# d-列名
# d-IFNULL
USE aepmis;

# 查询基本用户信息
DROP PROCEDURE IF EXISTS query_basic_user_info;
CREATE PROCEDURE query_basic_user_info(
    IN user_type VARCHAR(3),
    IN user_id VARCHAR(11),
#     IN password VARCHAR(20),
    OUT username VARCHAR(40),
    OUT permission_name VARCHAR(10)
#     OUT is_system_user BOOLEAN
)
BEGIN
    IF (user_type = '学生')
    THEN
        SELECT student.name INTO username FROM student WHERE student.student_id = user_id;
    ELSEIF (user_type = '教职工')
    THEN
        SELECT staff.name INTO username FROM staff WHERE staff.staff_id = user_id;
        SELECT permission.permission_name
        INTO permission_name
        FROM permission_assignment
                 INNER JOIN permission
                            ON permission_assignment.permission_id = permission.permission_id
        WHERE permission_assignment.staff_id = user_id;
    END IF;
END;

# 检查当前用户是否为系统用户
DROP PROCEDURE IF EXISTS check_whether_is_system_user;
CREATE PROCEDURE check_whether_is_system_user(
    IN user_type VARCHAR(3),
    IN user_id VARCHAR(11),
    IN password VARCHAR(20),
    OUT is_system_user BOOLEAN
)
BEGIN
    DECLARE num_of_user_found INTEGER DEFAULT 0;
    SET is_system_user = FALSE;

    IF (user_type = '学生')
    THEN
        SELECT COUNT(student.student_id)
        INTO num_of_user_found
        FROM student
        WHERE student.student_id = user_id
          AND student.password = password;
        IF (num_of_user_found = 1)
        THEN
            SET is_system_user = TRUE;
        ELSE
            SET is_system_user = FALSE;
        END IF;
    ELSEIF (user_type = '教职工')
    THEN
        SELECT COUNT(staff.staff_id)
        INTO num_of_user_found
        FROM staff
        WHERE staff.staff_id = user_id
          AND staff.password = password;
        IF (num_of_user_found = 1)
        THEN
            SET is_system_user = TRUE;
        ELSE
            SET is_system_user = FALSE;
        END IF;
    END IF;
END;

# # test
# DROP PROCEDURE IF EXISTS test;
# CREATE PROCEDURE test(
#     IN user_type VARCHAR(3),
#     IN user_id VARCHAR(11),
#     IN password VARCHAR(20)
# )
# BEGIN
#     DECLARE is_system_user BOOLEAN DEFAULT FALSE;
#     CALL check_whether_is_system_user(user_type, user_id, password, is_system_user);
#     SELECT is_system_user;
# #     DECLARE username VARCHAR(40) DEFAULT NULL;
# #     DECLARE permission_name VARCHAR(10) DEFAULT NULL;
# END;
# CALL test('学生', '20172002000', 'wxyfakjnoziswxy12');
# CALL test('学生', '20172002002', 'fsfwdo32jw1');

# # test
# DROP PROCEDURE IF EXISTS test;
# CREATE PROCEDURE test()
# BEGIN
#     DECLARE ans DECIMAL(4, 2);
#     SELECT 9 / 10 INTO ans;
#     SELECT ans;
# END;
# CALL test();

# 检查指定用户是否具备指定权限
DROP PROCEDURE IF EXISTS check_whether_has_permission;
CREATE PROCEDURE check_whether_has_permission(
    IN permission_name VARCHAR(10),
    IN staff_id CHAR(5),
    OUT has_permission BOOLEAN
)
BEGIN
    DECLARE num_of_record_found INTEGER DEFAULT -1;
    SELECT COUNT(*)
    INTO num_of_record_found
    FROM permission_assignment
             INNER JOIN permission
                        ON permission_assignment.permission_id = permission.permission_id
    WHERE permission.permission_name = permission_name
      AND permission_assignment.staff_id = staff_id;
    IF (num_of_record_found = 1)
    THEN
        SET has_permission = TRUE;
    ELSE
        SET has_permission = FALSE;
    END IF;
END;
# CALL check_whether_has_permission('管理员', '00101');
# 如果count(*)等于1，则说明该用户具备该权限

# 更改密码
DROP PROCEDURE IF EXISTS change_password;
CREATE PROCEDURE change_password(
    IN user_type VARCHAR(11),
    IN user_id VARCHAR(11),
    IN new_password VARCHAR(20)
)
BEGIN
    IF (user_type = '学生')
    THEN
        UPDATE student
        SET student.password = new_password
        WHERE student.student_id = user_id;
    ELSEIF (user_type = '教职工')
    THEN
        UPDATE staff
        SET staff.password = new_password
        WHERE staff.staff_id = user_id;
    END IF;
END;
# CALL change_password('staff', '00100', 'mxc091803984mxcc');
# CALL change_password('student', '20172002000', 'wxyfakjnoziswxy12');
